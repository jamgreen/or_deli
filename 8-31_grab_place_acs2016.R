#city level descriptive stats

if(!require(pacman)){install.packages("pacman"); library(pacman)}
p_load(tidycensus, tigris, dplyr, tidyr, sf)
options(tigris_class = "sf")

acs_vars <- tidycensus::load_variables(2014, "acs5", cache = TRUE)

#grab mhi, median age, race/ethnicity, tot pop, and pop_density-------

pop_vars <- c("B01002_001E", "B03002_001E",
              "B03002_002E", "B03002_003E", "B03002_012E",
              "B05002_013E", "B19013_001E")

place_vars <- get_acs(geography = "place", variables = pop_vars, year = 2016, 
                      state = "or", output = "wide")

or_places <- places(state = "or")

place_vars <- place_vars %>% 
  rename( median_age = B01002_001E,
          tot_pop = B03002_001E,
          not_hisp = B03002_002E,
          white_alone = B03002_003E,
          hispanic = B03002_012E,
          foreign = B05002_013E,
          mhi = B19013_001E)

place_vars <- place_vars %>% left_join(or_places %>% select(PLACEFP, GEOID), by = "GEOID")
place_vars <- st_as_sf(place_vars)

place_vars2992 <- st_transform(place_vars, crs = 2992)
place_vars2992 <- place_vars2992 %>% 
  mutate(area_mi = st_area(geometry)/27878000)

place_vars2992 <- place_vars2992 %>% 
  select(-starts_with("B0"))

place_vars2992 <- place_vars2992 %>% 
  mutate(pop_density = tot_pop/area_mi,
         not_hisp_per = not_hisp/tot_pop,
         white_per = white_alone/tot_pop,
         hisp_per = hispanic/tot_pop,
         foreign_per = foreign/tot_pop,
         non_white_per = (tot_pop - white_alone)/tot_pop)

#wants data for PDX, SHerwood, Newberg---------

pdx <- place_vars2992 %>% 
  filter(NAME == "Sherwood city, Oregon"|
         NAME == "Newberg city, Oregon"|
         NAME == "Portland city, Oregon")

saveRDS(pdx, "data/portland_sherwood_newberg_city_ACS.RDS")
